$(function () {
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();
  $('.carousel').carousel({
    interval: 3000
    });
  $('#contacto').on('show.bs.modal', function (e){
    console.log('el modal se está mostrando');
    $('#contactoBtn').removeClass('btn-success');
    $('#conctactBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', true);
  });
  $('#contacto').on('shown.bs.modal', function (e){
    console.log('el modal se está mostro');
  });
  $('#contacto').on('hide.bs.modal', function (e){
    console.log('el modal se oculta');
  });
  $('#contacto').on('hidden.bs.modal', function (e){
    console.log('el modal se oculto');
    $('#contactoBtn').prop('disabled', false);
  });
});